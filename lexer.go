package scpt

import (
	"github.com/alecthomas/participle/lexer"
	"github.com/alecthomas/participle/lexer/stateful"
)

// Create custom stateful regex lexer
var scptLexer = lexer.Must(stateful.NewSimple([]stateful.Rule{
	{"Ident", `[a-zA-Z_]\w*`, nil},
	{"String", `"[^"]*"`, nil},
	{"Number", `(?:\d*\.)?\d+`, nil},
	{"Punct", `[![@$&(){}\|:;"',.?]|]`, nil},
	{"Whitespace", `[ \t\r\n]+`, nil},
	{"Comment", `(###(.|\n)+###|#[^\n]+)`, nil},
	{"Operator", `(>=|<=|>|<|==|!=)|[-+*%/^]`, nil},
}))
