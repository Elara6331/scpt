package main

import (
	"errors"
	"fmt"
	"github.com/gen2brain/dlgs"
	"os"
	"os/exec"
)

// Default function to display a dialog
func displayDialog(args map[string]interface{}) (interface{}, error) {
	// Get title
	title, ok := args["title"]
	if !ok {
		return nil, errors.New("title not provided")
	}
	// Get unnamed argument as text
	text, ok := args[""]
	if !ok {
		return nil, errors.New("text not provided")
	}
	// Display correct dialog based on given type
	switch args["type"] {
	case "yesno":
		// Display yes or no dialog, returning bool based on user input
		return dlgs.Question(fmt.Sprint(title), fmt.Sprint(text), true)
	case "info":
		// Display info dialog, returning bool based on success
		return dlgs.Info(fmt.Sprint(title), fmt.Sprint(text))
	case "error":
		// Display error dialog, returning bool based on success
		return dlgs.Error(fmt.Sprint(title), fmt.Sprint(text))
	case "entry":
		// Check if default text given
		defaultText, ok := args["default"]
		if !ok {
			// Set to empty if not given
			defaultText = ""
		}
		// Display entry dialog
		input, _, err := dlgs.Entry(fmt.Sprint(title), fmt.Sprint(text), fmt.Sprint(defaultText))
		// Return user input
		return input, err
	default:
		// If type unknown, return error
		return nil, fmt.Errorf("unknown dialog type: %v", args["type"])
	}
}

// Default function to run a shell script using `sh -c`
func doShellScript(args map[string]interface{}) (interface{}, error) {
	// Get unnamed argument and assert its type as string
	script, ok := args[""].(string)
	// If assertion successful
	if ok {
		// Create new exec.Cmd containing `sh -c <script>`
		cmd := exec.Command("sh", "-c", script)
		// Set command I/O
		cmd.Stdout = os.Stdout
		cmd.Stdin = os.Stdin
		cmd.Stderr = os.Stderr
		// Run command
		_ = cmd.Run()
		return "", nil
	} else {
		return nil, errors.New("script not provided")
	}
}
