package main

import (
	"errors"
	"flag"
	"fmt"
	"gitea.arsenm.dev/Arsen6331/scpt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	useStdin := flag.Bool("stdin", false, "Parse STDIN")
	dumpAST := flag.Bool("dump-ast", false, "Dump the AST as JSON to STDOUT and quit")
	loadAST := flag.String("load-ast", "", "Load JSON AST from specified file and execute it")
	flag.Parse()

	var ast *scpt.AST

	if *loadAST != "" {
		data, err := ioutil.ReadFile(*loadAST)
		if err != nil {
			log.Fatalln("Error opening specified file:", err)
		}
		ast, err = scpt.LoadAST(data)
		if err != nil {
			log.Fatalln("Error loading AST:", err)
		}
	} else if *useStdin {
		var err error
		ast, err = scpt.Parse(os.Stdin)
		if err != nil {
			log.Fatalln("Error parsing STDIN:", err)
		}
	} else {
		if flag.NArg() < 1 {
			log.Fatalln("Filepath or --stdin required")
		}
		filePath := flag.Args()[0]
		file, err := os.Open(filePath)
		if err != nil {
			log.Fatalln("Error opening specified file:", err)
		}
		ast, err = scpt.Parse(file)
		if err != nil {
			log.Fatalln("Error parsing file:", err)
		}
	}

	if *dumpAST {
		data, err := ast.DumpPretty()
		if err != nil {
			log.Fatalln("Error dumping AST:", err)
		}
		fmt.Println(string(data))
		os.Exit(0)
	}

	scpt.AddFuncs(scpt.FuncMap{
		"print":           scptPrint,
		"display-dialog":  displayDialog,
		"do-shell-script": doShellScript,
	})

	err := ast.Execute()
	if err != nil {
		log.Fatalln("Error executing script:", err)
	}
}

func scptPrint(args map[string]interface{}) (interface{}, error) {
	val, ok := args[""]
	if !ok {
		return nil, errors.New("print requires an unnamed argument")
	}
	fmt.Println(val)
	return nil, nil
}
