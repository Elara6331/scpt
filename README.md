# scpt

scpt is an applescript-inspired scripting language written for fun and to see if I could.

[![Go Reference](https://pkg.go.dev/badge/gitea.arsenm.dev/Arsen6331/scpt.svg)](https://pkg.go.dev/gitea.arsenm.dev/Arsen6331/scpt)

---

### Usage

scpt is to be used as a library imported into Go. A basic interpreter with no extra functionality would look like this:

```go
package main

import (
	"gitea.arsenm.dev/Arsen6331/scpt"
	"log"
	"os"
	"path/filepath"
)

func main() {
	filename := os.Args[1]
	file, err := os.Open(filepath.Clean(filename))
	if err != nil {
		log.Fatalln(err)
	}
	defer file.Close()
	ast, err := scpt.Parse(file)
	if err != nil {
		log.Fatalln(err)
	}
	err = ast.Execute()
	if err != nil {
		log.Fatalln(err)
	}
}
```

---

### Basic Syntax

The basic syntax of scpt can be learned from the test.scpt file.

---

### Default Functions

scpt comes with the following default functions:

- `str`: Convert value to string
- `num`: Parse string to number (`float64`)
- `bool`: Parse string to boolean
- `break`: Break out of loop (Errors if not in loop)
- `append`: Return an array with given items appended
- `exit`: Exit with given exit code
- `return`: Return value in function (Errors if not within function)
- `print`: Print using `fmt.Println()`

---

### Adding functionality:

Adding functionality is simple and requires a call to `scpt.AddFuncs()` or `scpt.AddVars()`. Here are some examples:

```go
scpt.AddFuncs(scpt.FuncMap{
	"my-function": myFunction
})
```
Where `myFunction` is:
```go
func myFunction(args map[string]interface{}) (interface{}, error) {
	fmt.Println(args)
	return nil, nil
}
```

After the call to `scpt.AddFuncs()`, `my-function` can be used to run the function from within an scpt script. Variables work similarly.