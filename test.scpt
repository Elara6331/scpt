#!/usr/bin/env scpt

set y to (display-dialog "Hello" with title 12 with type "yesno")
display-dialog "Goodbye" with title 21 with type "error"
do-shell-script "notify-send Test Notification"
do-shell-script {"echo " + (display-dialog {"Test " + "2"} with title 30 with type "entry" with default "text")}
set x to $y

if {3 == 3} {
    set x to "hi"
    do-shell-script {"echo " + $x}
    display-dialog "3 is equal to 3!" with title "New Discovery" with type "info"
    if {3 == 4} {
        display-dialog "What? Why is 3 equal to 4?" with title "What?!" with type "error"
        do-shell-script "echo hi"
    }
}

repeat 5 times { z in
    print $z
}

print (str 10.5)

if (bool "true") {
    print "True"
}

###
This is a multiline comment
###

# This is a single line comment

set hi to ["test", 3]

set hi[0] to "test2"

print $hi[0]

set hi to (append $hi with items [5, 4])

print {$hi[2] + $hi[3]}

set msi to [5: "hi", "hello": "world"]
set msi[5] to "hello"
print $msi[5]
print $msi["hello"]

set c to 0
set f to true
loop while $f {
    set c to {$c + 1}
    if {$c == 3} {
        set f to false
    }
    print {"iter: " + (str $c)}
}

repeat 6 times { i in
    print {"brktest: " + (str $i)}
    if {$i == 3} {
            break
    }
    print {"brktest: " + (str $i) + " (2)"}
}

define hello-x {
    print {"Hello, " + $_args[""]}
    return {"Hello, " + $_args[""]}
    print "Something isn't right"
}

hello-x "Function"
set f to (hello-x "World")
print {"Returned: " + $f}