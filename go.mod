module gitea.arsenm.dev/Arsen6331/scpt

go 1.16

require (
	github.com/alecthomas/participle v0.7.1
	github.com/antonmedv/expr v1.8.9
	github.com/gen2brain/dlgs v0.0.0-20210222160047-2f436553172f
)
